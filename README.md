# README #

This is  sample of how to generate a sunburst chart with D3.

### Data Explanation ###

The chart is usually used to show Data following path. It's an Array of Arrays.

* The first element is text, contains from father to the last son separated by coma.
* The second element is the amount.

Note: The filtering data is to match with colour.

### How it looks? ###

![image_sunburst.png](https://bitbucket.org/repo/gqEejA/images/3245600917-image_sunburst.png)
/**
 * Created by Marc.Rivelles on 08/02/2016.
 */
(function(){
    var sunburst = {};

    sunburst.idChart = "#chart_sunburst";
    sunburst.idLegend = "#legend_sunburst";
    sunburst.idSeq = "#sequence_sunburst";
    sunburst.idSeq2 = "#sequence_sunburst2";
    sunburst.idExplain = "#explanation_sunburst";
    sunburst.idPerc = "#percentage_sunburst";
    sunburst.idToggle = "#togglelegend_sunburst";

    sunburst.width = 750;
    sunburst.height = 600;
    sunburst.m_w = 0;

    // Mapping of step names to colors.
    sunburst.colors = {};
    sunburst.data = [];


    sunburst.ordinal_numbers = [
        "First",
        "Second",
        "Third",
        "Fourth",
        "Fifth",
        "Sixth",
        "Seventh",
        "Eighth",
        "Ninth",
        "Tenth",
        "Eleventh",
        "Twelfth",
        "Thirteenth",
        "Fourteenth",
        "Fifteenth",
        "Sixteenth",
        "Seventeenth",
        "Eighteenth",
        "Nineteenth",
        "Twentieth",
        "Twenty-First",
        "Twenty-Second",
        "Twenty-Third",
        "Twenty-Fourth",
        "Twenty-Fifth",
        "Twenty-Sixth",
        "Twenty-Seventh",
        "Twenty-Eighth",
        "Twenty-Ninth",
        "Thirtieth",
        "Thirty-First",
        "Thirty-Second",
        "Thirty-Third",
        "Thirty-Fourth",
        "Thirty-Fifth",
        "Thirty-Sixth",
        "Thirty-Seventh",
        "Thirty-Eighth",
        "Thirty-Ninth",
    ];
    // Breadcrumb dimensions: width, height, spacing, width of tip/tail.
    var b = {
        w: 75, h: 30, s: 3, t: 10
    };

    sunburst.arc = {};
    sunburst.radius = {};
    sunburst.totalSize = {};
    sunburst.vis = {};
    sunburst.partition = {};
    sunburst.filtering = {};

    sunburst.draw = function(){
        d3.select(sunburst.idChart + " svg").remove();
        sunburst.radius = Math.min(sunburst.width, sunburst.height) / 2;

        sunburst.totalSize = 0;

        sunburst.vis = d3.select(sunburst.idChart).append("svg:svg")
            .attr("width", sunburst.width)
            .attr("height", sunburst.height)
            .append("svg:g")
            .attr("id", "container")
            .attr("transform", "translate(" + sunburst.width / 2 + "," + sunburst.height / 2 + ")");

        sunburst.partition = d3.layout.partition()
            .size([2 * Math.PI, sunburst.radius * sunburst.radius])
            .value(function(d) { return d.size; });

        sunburst.arc = d3.svg.arc()
            .startAngle(function(d) { return d.x; })
            .endAngle(function(d) { return d.x + d.dx; })
            .innerRadius(function(d) { return Math.sqrt(d.y); })
            .outerRadius(function(d) { return Math.sqrt(d.y + d.dy); });

        var json = buildHierarchy2(sunburst.data);
        createVisualization(json);
    }

    // Main function to draw and set up the visualization, once we have the data.
    var createVisualization = function(json) {

        // Basic setup of page elements.
        initializeBreadcrumbTrail();
        drawLegend();
        d3.select(sunburst.idToggle).on("click", toggleLegend);

        // Bounding circle underneath the sunburst, to make it easier to detect
        // when the mouse leaves the parent g.
        sunburst.vis.append("svg:circle")
            .attr("r", sunburst.radius)
            .style("opacity", 0);

        // For efficiency, filter nodes to keep only those large enough to see.
        var nodes = sunburst.partition.nodes(json)
            .filter(function(d) {
               return ((d.dx > 0.005) && d.name!= "$BLANK$"); // 0.005 radians = 0.29 degrees
            });

        var path = sunburst.vis.data([json]).selectAll("path")
            .data(nodes)
            .enter().append("svg:path")
            .attr("display", function(d) { return d.depth ? null : "none"; })
            .attr("d", sunburst.arc)
            .attr("fill-rule", "evenodd")
            .style("fill", function(d) { return sunburst.colors[d.name]; })
            .style("opacity", 1)
            .on("mouseover", mouseover);

        // Add the mouseleave handler to the bounding circle.
        d3.select("#container").on("mouseleave", mouseleave);
        //Explanation Sunburst
        setupExpSun();
        // Get total size of the tree = value of root node from partition.
        sunburst.totalSize = path.node().__data__.value;
    };


    var setupExpSun = function(){
        var f_circle = d3.selectAll('#container path')[0][1];
        var radius = parseFloat(f_circle.attributes.d.value.slice(3, f_circle.attributes.d.value.indexOf("A")));
        d3.select(sunburst.idExplain)
            .style({
                top: sunburst.height/2 - 52 + "px",
                left: sunburst.width/2 - 62 + "px",
                width: 140 + "px",
            });

    }
    // Take a 2-column CSV and transform it into a hierarchical structure suitable
    // for a partition layout. The first column is a sequence of step names, from
    // root to leaf, separated by hyphens. The second column is a count of how
    // often that sequence occurred.
    var root = {name: "root", children: []};
    var buildHierarchy2 = function(csv) {
        var color_array = [];
        sunburst.colors = {};
        sunburst.colors["Conversion"] = "#000000";
        var demo_colours = {};
        for (var i = 0; i < csv.length; i++) {
            var sequence = csv[i][0];
            var parts = sequence.split(",");
            for (var j = 0; j < parts.length; j++) {
                if (color_array.indexOf(parts[j]) == -1) {
                    color_array.push(parts[j]);
                    var idx_father = fKey(sunburst.filtering, "ch_", parts[j].slice(0, parts[j].indexOf(" - ")) + "_");
                    var idx_st_tc = fKey(sunburst.filtering[idx_father].sons, "tc", parts[j].slice(parts[j].indexOf(" - ") + 3, parts[j].length));
                    demo_colours[parts[j]] = sunburst.filtering[idx_father].sons[idx_st_tc].colour;
                }
            }
        }

        //Sort Colours
        color_array.sort();

        for(var i=0;i<color_array.length;i++){
            sunburst.colors[color_array[i]] = demo_colours[color_array[i]]
        }

        root = {name: "root", children: []};
        for (var i = 0; i < csv.length; i++) {
            var sequence = csv[i][0];
            var parts = sequence.split(",");
            var size = csv[i][1];

            var idx = fKey(root.children, "name", parts[0]);
            if(idx != null){
                //Checks this sons are Ok to append the path
                //root.children[idx] = findSon(root.children[idx], parts.slice(1), size);
                var done = findSon2(root.children[idx], parts.slice(1), size)
                if(!done){
                    root.children.push(parts.length > 1 ?
                    {
                        name: parts[0],
                        children:  [createSons(parts.slice(1), size)]
                    }
                        :
                    {
                        name: parts[0],
                        size: size
                    });
                }


            }else{
                root.children.push(parts.length > 1 ?
                {
                    name: parts[0],
                    children:  [createSons(parts.slice(1), size)]
                }
                    :
                {
                    name: parts[0],
                    size: size
                });
            }
        }

        //Rellocate Sum of All sons to Fathers

        root.size = SumSons(root);

        FatherBlank(root);


        return {name: "root", children: root.children.slice(0, root.children.length - 1), size: root.size};
    };

    var FatherBlank = function(father){
        var size = 0;

        if(typeof father.children != "undefined") {
            for (var i = 0; i < father.children.length; i++) {
                FatherBlank(father.children[i]);
                size += father.children[i].size;
            }
            if(father.size != size){//Append Blank Children
                father.children.push({
                    name: "$BLANK$",
                    size: size > father.size ? (size - father.size) : (father.size - size)
                });
            }
        }
    }

    var SumSons = function(father){
        var size = 0;

        if(typeof father.size == "undefined"){
            size = 0;
            father.size = 0;
        }else{
            size = father.size;
        }

        if(typeof father.children != "undefined"){
            for (var i = 0; i < father.children.length; i++) {
                size += SumSons(father.children[i]);
            }
            father.size = size;
        }

        return size;
    }

    var findSon2 = function(children, sons, size){
        if(typeof children.children != "undefined"){
            var idx = fKey(children.children, "name", sons[0]);
            if(idx != null){
                var done = findSon2(children.children[idx], sons.slice(1), size);
                return done;
            }else{
                children.children.push(sons.length > 1 ?
                {
                    name: sons[0],
                    children:  [createSons(sons.slice(1), size)]
                }
                    :
                {
                    name: sons[0],
                    size: size
                });

                return true;
            }
        }else{
            children.children = [sons.length > 1 ?
            {
                name: sons[0],
                children:  [createSons(sons.slice(1), size)]
            }
                :
            {
                name: sons[0],
                size: size
            }];

            return true;
        }

    }

    var createSons = function(parts, size){
        return parts.length > 1 ?
        {
            name: parts[0], children: [createSons(parts.slice(1), size)]
        }
            :
        {
            name: parts[0], size: size
        };
    };

    // Fade all but the current sequence, and show it in the breadcrumb trail.
    var mouseover = function(d) {

        var percentage = (100 * d.value / sunburst.totalSize).toPrecision(3);
        var percentageString = percentage + "%";
        if (percentage < 0.1) {
            percentageString = "< 0.1%";
        }

        d3.select(sunburst.idPerc)
            .text(percentageString);

        d3.select(sunburst.idExplain)
            .style("visibility", "");

        var sequenceArray = getAncestors(d);
        updateBreadcrumbs(sequenceArray, percentageString);

        // Fade all the segments.
        d3.selectAll(sunburst.idChart + " path")
            .style("opacity", 0.3);

        // Then highlight only those that are an ancestor of the current segment.
        sunburst.vis.selectAll(sunburst.idChart + " path")
            .filter(function(node) {
                return (sequenceArray.indexOf(node) >= 0);
            })
            .style("opacity", 1);
    }

// Restore everything to full opacity when moving off the visualization.
    var mouseleave = function(d) {

        // Hide the breadcrumb trail
        d3.select("#trail")
            .style("visibility", "hidden");

        d3.select("#trail2")
            .style("visibility", "hidden");

        // Deactivate all segments during transition.
        d3.selectAll(sunburst.idChart + " path").on("mouseover", null);

        // Transition each segment to full opacity and then reactivate it.
        d3.selectAll(sunburst.idChart + " path")
            .transition()
            .duration(1000)
            .style("opacity", 1)
            .each("end", function() {
                d3.select(this).on("mouseover", mouseover);
            });

        d3.select(sunburst.idExplain)
            .style("visibility", "hidden");
    }

    // Given a node in a partition layout, return an array of all of its ancestor
    // nodes, highest first, but excluding the root.
    var getAncestors = function(node) {
        var path = [];
        var current = node;
        while (current.parent) {
            path.unshift(current);
            current = current.parent;
        }
        return path;
    }

    var initializeBreadcrumbTrail = function() {
        // Add the svg area.
        d3.select(sunburst.idSeq + " svg").remove();
        var trail = d3.select(sunburst.idSeq).append("svg:svg")
            .attr("width", sunburst.width)
            .attr("height", 100)
            .attr("id", "trail");
        // Add the label at the end, for the percentage.
        trail.append("svg:text")
            .attr("id", "endlabel")
            .style("fill", "#000");

        d3.select(sunburst.idSeq2 + " svg").remove();
        var trail2 = d3.select(sunburst.idSeq2).append("svg:svg")
            .attr("width", sunburst.width)
            .attr("height", 250)
            .attr("id", "trail2");
        // Add the label at the end, for the percentage.
        trail2.append("svg:text")
            .attr("id", "ordinal_seq0")
            .style("fill", "#831A81")
            .style("font-size", "35px");

        trail2.append("svg:text")
            .attr("id", "ordinal_seq1")
            .style("fill", "#831A81")
            .style("font-size", "35px");
    }

// Generate a string that describes the points of a breadcrumb polygon.
    var breadcrumbPoints = function(d, i) {
        var points = [];
        points.push("0,0");
        points.push(b.w + ",0");
        points.push(b.w + b.t + "," + (b.h / 2));
        points.push(b.w + "," + b.h);
        points.push("0," + b.h);
        /*if (i > 0) { // Leftmost breadcrumb; don't include 6th vertex.
            points.push(b.t + "," + (b.h / 2));
        }*/
        points.push(b.t + "," + (b.h / 2));
        return points.join(" ");
    }

    // Update the breadcrumb trail to show the current sequence and percentage.
    var updateBreadcrumbs = function(nodeArray, percentageString) {
        var nodeArray_reverse = [],
            depth = 1;
        for(var i=nodeArray.length - 1; i>-1; i--){
            nodeArray_reverse.push(nodeArray[i]);
            nodeArray_reverse[depth - 1].depth = depth;
            depth++;
        }
        nodeArray_reverse.push({
            name: "Conversion", depth: depth
        });
        // Data join; key function combines name and depth (= position in sequence).
        var g = d3.select("#trail")
            .selectAll("g")
            //.data(nodeArray, function(d) { return d.name + d.depth; });
            .data(nodeArray_reverse, function(d) { return d.name + d.depth; });

        // Add breadcrumb and label for entering nodes.
        var entering = g.enter().append("svg:g");

        entering.append("svg:polygon")
            .attr("points", breadcrumbPoints)
            .style("fill", function(d) { return sunburst.colors[d.name]; });

        entering.append("svg:text")
            .attr("x", (b.w + b.t) / 2)
            .attr("y", b.h / 2)
            .attr("dy", "0.35em")
            .attr("text-anchor", "middle")
            .text(function(d) { return d.name; });

        // Set position for entering and updating nodes.
        //Check how to jump of line
        var yAxis = 0,
            xAxis = -1;
        g.attr("transform", function(d, i) {
            if(((xAxis + 1) * (b.w + b.s))/sunburst.width > 0.9){
                yAxis += b.h + 5;
                xAxis = 0;
            }else{
                xAxis += 1;
            }
            return "translate(" + xAxis * (b.w + b.s) + ", " + yAxis + ")";
        });

        // Remove exiting nodes.
        g.exit().remove();

        // Now move and update the percentage at the end.
        d3.select("#trail").select("#endlabel")
            //.attr("x", (nodeArray.length + 0.5) * (b.w + b.s))
            .attr("x", (xAxis + 1.2) * (b.w + b.s))
            //.attr("y", b.h / 2)
            .attr("y", b.h / 2 + yAxis)
            .attr("dy", "0.35em")
            .attr("text-anchor", "middle")
            .text(percentageString);


        d3.select("#trail2").select("#ordinal_seq0")
            .attr("dy", "1.35em")
            .attr("text-anchor", "middle")
            .attr("transform", "translate(" + 250 + ", " + 0 + ")")
            .text(sunburst.ordinal_numbers[nodeArray.length - 1] + " Click");

        d3.select("#trail2").select("#ordinal_seq1")
            .attr("dy", "1.35em")
            .attr("text-anchor", "middle")
            .attr("transform", "translate(" + 250 + ", " + 60 + ")")
            .text("Before Conversion");

        // Make the breadcrumb trail visible, if it's hidden.
        d3.select("#trail")
            .style("visibility", "");

        d3.select("#trail2")
            .style("visibility", "");

    }

    var drawLegend = function() {

        // Dimensions of legend item: width, height, spacing, radius of rounded rect.
        var li = {
            w: 75, h: 30, s: 3, r: 3
        };
        d3.select(sunburst.idLegend + " svg").remove();
        var legend = d3.select(sunburst.idLegend).append("svg:svg")
            .attr("width", li.w)
            .attr("height", d3.keys(sunburst.colors).length * (li.h + li.s));

        var g = legend.selectAll("g")
            .data(d3.entries(sunburst.colors))
            .enter().append("svg:g")
            .attr("transform", function(d, i) {
                return "translate(0," + i * (li.h + li.s) + ")";
            });

        g.append("svg:rect")
            .attr("rx", li.r)
            .attr("ry", li.r)
            .attr("width", li.w)
            .attr("height", li.h)
            .style("fill", function(d) { return d.value; });

        g.append("svg:text")
            .attr("x", li.w / 2)
            .attr("y", li.h / 2)
            .attr("dy", "0.35em")
            .attr("text-anchor", "middle")
            .text(function(d) { return d.key; });

        //Find max width
        var txts = g.selectAll("text");
        sunburst.m_w = 0;
        for(var i=0; i<txts.length; i++){
            var box = txts[i][0].getBBox();
            if(box.width > sunburst.m_w){
                sunburst.m_w = box.width;
            }
        }
        sunburst.m_w += 20;

        d3.select(sunburst.idLegend + " svg")
            .attr("width", sunburst.m_w);

        g.selectAll("rect")
            .attr("width", sunburst.m_w);

        g.selectAll("text")
            .attr("x", sunburst.m_w / 2);

        b.w = sunburst.m_w;
    };

    var toggleLegend = function() {
        var legend = d3.select(sunburst.idLegend);
        if (legend.style("visibility") == "hidden") {
            legend.style("visibility", "");
        } else {
            legend.style("visibility", "hidden");
        }
    };
    sunburst.version = "1.0";

    /************************************
     Exposing Sunburst
     ************************************/
    var hasModule = (typeof module !== 'undefined' && module.exports);
    // CommonJS module is defined
    if (hasModule) {
        module.exports = sunburst;
    }

    /*global ender:false */
    if (typeof ender === 'undefined') {
        // here, `this` means `window` in the browser, or `global` on the server
        // add `sunburst` as a global object via a string identifier,
        // for Closure Compiler 'advanced' mode
        this['sunburst'] = sunburst;
    }

    /*global define:false */
    if (typeof define === 'function' && define.amd) {
        define([], function () {
            return sunburst;
        });
    }
}).call(this);